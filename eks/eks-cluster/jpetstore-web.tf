resource "kubernetes_deployment" "jpetstoreweb" {
    metadata {
       name = "jpetstoreweb"
    }
    spec {
        replicas = 1
        selector {
         match_labels = {
             app: "jpetstoreweb"
         }          
        }
        template {
            metadata {
             labels = {
              app = "jpetstoreweb"
             }
             annotations = {
               "sidecar.isto.io/inject" = "true"
             }
        }
        spec {
          container {
            name = "jpetstoreweb"
            image = "public.ecr.aws/a0g8x7v9/pdp-repository:latest"
            env {
              name = "VERSION"
              value = "1"
            }
            port {
              container_port = 9080 
            }
            readiness_probe {
             http_get {
              path = "/"
              port = 9080
             }
             initial_delay_seconds = 10
             period_seconds = 5 
            }
          } 
        }  
      } 
    }
}

resource "kubernetes_service" "web_service" {
    metadata {
        name = "web"
    }
    spec {
     selector = {
       app = "jpetstoreweb"
     }
     port {
         port = 80
         target_port = 9080
     }
     type = "LoadBalancer"
    }
      
}

resource "kubernetes_deployment" "jpetstoredb" {
    metadata {
       name = "jpetstoredb"
    }
    spec {
        replicas = 1
        selector {
         match_labels = {
             app: "jpetstoredb"
         }          
        }
        template {
            metadata {
             labels = {
              app = "jpetstoredb"
             }
             annotations = {
               "sidecar.isto.io/inject" = "true"
             }
        }
        spec {
          container {
            name = "jpetstoredb"
            image = "public.ecr.aws/a0g8x7v9/pdp-repository:latest"
            env {
              name = "MYSQL_ROOT_PASSWORD"
              value = "foobar"
            }
            env {
              name = "MYSQL_DATABASE"
              value = "jpetstore"
            }
            env {
              name = "MYSQL_USER"
              value = "jpetstore"
            }
            env {
              name = "MYSQL_PASSWORD"
              value = "foobar"
            }           
            port {
              container_port = 3306 
            }

          } 
        }  
      } 
    }
}

resource "kubernetes_service" "db_service" {
    metadata {
        name = "db"
    }
    spec {
     selector = {
       app = "jpetstoredb"
     }
     port {
         port = 3306
         target_port = 3306
     }
    }  
}
