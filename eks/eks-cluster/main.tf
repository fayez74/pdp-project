provider "aws" {
  region = local.region
}

locals {
  name            = "eks-cluster-jpetstore"
  cluster_version = "1.20"
  region          = "eu-west-1"
  vpc_id          = "vpc-06edaf9ad2464d046"
  subnet_1a = "subnet-09ab067301f73bc12"
  subnet_1b = "subnet-0b57dd929e5db0b85"
}


data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source          = "../terraform-aws-eks/"
  cluster_name    = "eks-jpetstore-cluster"
  cluster_version = "1.20"
  subnets         = ["subnet-09ab067301f73bc12", "subnet-0b57dd929e5db0b85"]
  vpc_id          = "vpc-06edaf9ad2464d046"

  node_groups = {
    public = {
      subnets          = ["subnet-09ab067301f73bc12"]
      desired_capacity = 2
      max_capacity     = 5
      min_capacity     = 1

      instance_type = "t2.small"
      k8s_labels = {
        Environment = "public"
      }
    }
  }

}