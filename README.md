# Pre-Requisites for Jpetstore EKS Setup
- Remote state setup (DynamoDB and S3 bucket)
### Install kubectl and configure on Jenkins
```
sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

### Install helm on Jenkins
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

## Deploy these components in order:

- remote state (local deployment)
- vpc (local deployment)
- Jenkins (local deployment)
- Setup Backups (Optional)
- Copy over all pipelines from pipelines directory
- ECR (ECR pipeline)
- Build docker image (ansible-docker pipeline)
- Build EKS (EKS pipeline)
- Run ansible monitoring pipelines (ansible-monitoring pipeline)
- Setup Grafana and add Dashboards

## Monitoring - Prometheus and Grafana
- This repo uses prometheus and Grafana to monitor the Jpetstore cluster. These commands will need to be run locally

### Prometheus Setup

```
kubectl create namespace prometheus

helm install prometheus prometheus-community/prometheus \
    --namespace prometheus \
    --set alertmanager.persistentVolume.storageClass="gp2" \
    --set server.persistentVolume.storageClass="gp2"

```

- Check the status of the prometheus resources:

```
kubectl get all -n prometheus

```

- To view the prometheus UI, run this on a new terminal:

```
kubectl port-forward -n prometheus deploy/prometheus-server 8080:9090

```

- In the browser, to view targets:

```
localhost:8080/targets

```
### Grafana Setup

- Create a local file called `grafana.yaml` and add the following commands:

```
datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      url: http://prometheus-server.prometheus.svc.cluster.local
      access: proxy
      isDefault: true

```

- Then run:

```
kubectl create namespace grafana

helm install grafana grafana/grafana \
    --namespace grafana \
    --set persistence.storageClassName="gp2" \
    --set persistence.enabled=true \
    --set adminPassword='admin' \
    --values grafana.yaml \
    --set service.type=LoadBalancer

```

- Check the status of the prometheus resources:

```
kubectl get all -n grafana

```

- To view the Grafana UI, enter:

```
export ELB=$(kubectl get svc -n grafana grafana -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')

echo "http://$ELB"

```

- When logging in, use the username admin and get the password hash by running the following:

```
kubectl get secret --namespace grafana grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

```
### Dashboards

- To Setup dashboards, login to Grafana and select the import tab on the left. The add the following codes to load in:

```
3119
6417
Select ‘Prometheus’ as the endpoint under prometheus data sources drop downs
Click ‘Import’

```