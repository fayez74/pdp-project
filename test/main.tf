terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "=3.30.0"
    }
  }
}

provider "aws" {
  region  = "eu-west-1"
}

resource "aws_s3_bucket" "bucketfortest" {
  bucket = "upload-bucket1234556-test"
  acl    = "private"

  tags = {
    Name = "upload-bucket"
  }
}

resource "aws_sqs_queue" "queue" {
  name                      = "upload-queue"
  delay_seconds             = 60
  max_message_size          = 8192
  message_retention_seconds = 172800
  receive_wait_time_seconds = 15

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "1",
  "Statement": [
    {
      "Sid": "1",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "arn:aws:sqs:*:*:upload-queue",
      "Condition": {
        "ArnEquals": { "aws:SourceArn": "${aws_s3_bucket.bucketfortest.arn}" }
      }
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_notification" "bucket_notif" {
  bucket = aws_s3_bucket.bucketfortest.id

  queue {
    queue_arn     = aws_sqs_queue.queue.arn
    events        = ["s3:ObjectCreated:*"]
  }
}
