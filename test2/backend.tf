terraform {
  backend "s3" {
    bucket = "terraform-remote-state-pdp"
    key = "terraform/test2/terraform.tfstate"
    region = "eu-west-2"
    dynamodb_table = "remote-state-locking-table"
    encrypt = "true"
  }
}