#!/usr/bin/env bash

# Print commands and their arguments as they are executed
set -x
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Install Docker

sudo yum update -y
sudo yum install -y docker
sudo service docker start
sudo systemctl enable docker
sudo usermod -a -G docker ec2-user

#Install Jenkins

sudo yum update –y
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo

sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

sudo yum install jenkins java-1.8.0-openjdk-devel -y
sudo service jenkins start

sudo usermod -a -G docker jenkins
sudo systemctl daemon-reload
sudo service jenkins start
sudo systemctl status jenkins

sudo service docker stop
sudo service docker start

sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R

sudo groupadd docker
sudo usermod -aG docker $USER

#Install Git
sudo yum install git -y

#Install terraform
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

#kubectl installed manually via ssh