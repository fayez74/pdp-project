resource "aws_security_group" "security_group" {
  name        = "Jenkins SG"
  description = "Allow Jenkins inbound traffic"
  vpc_id      = "vpc-06edaf9ad2464d046"
}
  
  resource "aws_security_group_rule" "ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}
#Adjusted as grafana and prometheus were interfering
  resource "aws_security_group_rule" "jenkins" {
  type              = "ingress"
  from_port         = 7777
  to_port           = 7777
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}

resource "aws_security_group_rule" "outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}