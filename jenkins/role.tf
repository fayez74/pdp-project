resource "aws_iam_role" "ec2_role" {
  name                = "jenkins_role"
  managed_policy_arns = [ 
    "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
    //AdministratorAccess added manually
  ]
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "AmazonEC2PublicContainerRegistryFullAccess" {
  name = "AmazonEC2PublicContainerRegistryFullAccess"
  role = aws_iam_role.ec2_role.name
  policy = jsonencode(
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ecr-public:GetAuthorizationToken",
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "ecr-public:*",
            "Resource": [
                "arn:aws:ecr-public::439452799995:repository/*",
                "arn:aws:ecr-public::439452799995:registry/*"
            ]
        }
    ]
    } 
  )
  
}

