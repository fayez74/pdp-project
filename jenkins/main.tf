provider "aws" {
    region =  "eu-west-1"
}

data "terraform_remote_state" "vpc_infrastructure" {
        backend = "s3"
        config = {
          bucket = var.remote_state_bucket
          key = "terraform/vpc/terraform.tfstate"
          region = "eu-west-2"
          
         }
  
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
    name = "${var.name}-profile"
    role = aws_iam_role.ec2_role.name
}

resource "aws_instance" "ec2_server_instance" {
    ami = "ami-02b4e72b17337d6c1"
    instance_type = "t3.medium"
    iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name
    key_name = "pdpkey"
    user_data = data.template_file.userdata.rendered
    subnet_id = "subnet-0b57dd929e5db0b85"
    vpc_security_group_ids = [ aws_security_group.security_group.id ]
    tags = {
        Name = var.name
        Environment = "Dev"
        Key = "pdpkey"    
    }
    root_block_device {
        delete_on_termination = true
        volume_size = 100
        volume_type = "gp2"
        tags = {
          "Name" = "rootVolume"
          "Encrypted" = "no"
        }
    }
}


data "template_file" "userdata" {
    template = "${file("userdata.sh")}"
  
}

