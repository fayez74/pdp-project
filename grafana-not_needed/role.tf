resource "aws_iam_role" "ec2_role" {
  name = "grafana_role"
  path = "/"
  managed_policy_arns = [ "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess" ]

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}
