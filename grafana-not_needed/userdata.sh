#!/usr/bin/env bash

# Print commands and their arguments as they are executed
set -x
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Update packages and install Grafana

sudo systemctl status grafana-server

if [ $? == 0 ]; then
    echo 'Grafana server exists'
    sudo /bin/systemctl start grafana-server.service
    echo 'Grafana server has started up'
else    
    sudo yum update -y

    sudo wget https://dl.grafana.com/enterprise/release/grafana-enterprise-8.2.3-1.x86_64.rpm

    sudo yum install grafana-enterprise-8.2.3-1.x86_64.rpm -y

    sudo /bin/systemctl daemon-reload
    sudo /bin/systemctl enable grafana-server.service
    sudo /bin/systemctl start grafana-server.service

fi