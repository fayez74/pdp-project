resource "aws_security_group" "security_group" {
  name        = "Grafana SG"
  description = "Allow Grafana inbound traffic"
  vpc_id      = "vpc-06edaf9ad2464d046"
}
  
  resource "aws_security_group_rule" "ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["81.129.12.155/32"]
  security_group_id = aws_security_group.security_group.id
}
  resource "aws_security_group_rule" "jenkins" {
  type              = "ingress"
  from_port         = 3000
  to_port           = 3000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}

resource "aws_security_group_rule" "outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}