provider "aws" {
  alias  = "us_east_1"
  region = "us-east-1"
}

resource "aws_ecrpublic_repository" "ecr-repo" {
  provider = aws.us_east_1
  repository_name = "pdp-repository"
}