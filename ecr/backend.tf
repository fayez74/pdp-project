terraform {
  backend "s3" {
    bucket = "terraform-remote-state-pdp"
    key = "terraform/ecr/terraform.tfstate"
    region = "eu-west-2"
    dynamodb_table = "remote-state-locking-table"
    encrypt = "true"
  }
}