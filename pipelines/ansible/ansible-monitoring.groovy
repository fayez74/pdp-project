pipeline {
    agent any
    environment {
        AWS_DEFAULT_REGION="eu-west-1"
    }

    stages {
        
        stage('clone') {
            steps {
                cleanWs()
                sh 'git clone https://fayez74@bitbucket.org/fayez74/ansible.git'
                sh 'cd ansible && ls -la'
            }
        }
        stage('CLear kube workspace') {
            steps {
                sh 'kubectl delete ns prometheus'
                sh 'kubectl delete ns grafana'
            }
        }
        stage('Run Playbook') {
            steps {
                dir("$WORKSPACE/ansible") {
                    
                    sh '''
                    export ANSIBLE_HOST_KEY_CHECKING=False
                    export ANSIBLE_RETRY_FILES_ENABLED="False"
                    
                    ansible-playbook -i "localhost," monitoring.yml
                    '''
                }
             // input 'Do you want to Apply changes?'
            }
        }
        
    }
}