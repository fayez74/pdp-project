pipeline {
    agent any
    environment {
        AWS_DEFAULT_REGION="eu-west-1"
    }

    stages {
        
        stage('clone') {
            steps {
                cleanWs()
                sh 'git clone https://fayez74@bitbucket.org/fayez74/pdp-project.git'
                sh 'cd pdp-project/backups && ls -la'
            }
        }
        stage('Terraform Plan') {
            steps {
                dir("$WORKSPACE/pdp-project/backups") {
                    sh '''
                    terraform init
                    terraform plan -destroy
                    '''
                }
              input 'Do you want to Apply changes?'
            }
        }
        stage('Terraform Apply') {
            steps {
                dir("$WORKSPACE/pdp-project/backups") {
                    sh '''
                    terraform destroy --auto-approve
                    '''
                }
            }
        }

    }
}