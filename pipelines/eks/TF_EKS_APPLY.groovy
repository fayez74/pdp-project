pipeline {
    agent any
    environment {
        AWS_DEFAULT_REGION="eu-west-1"
    }

    stages {
        
        stage('clone') {
            steps {
                cleanWs()
                sh 'git clone https://fayez74@bitbucket.org/fayez74/pdp-project.git'
                sh 'cd pdp-project/eks/eks-cluster && ls -la'
            }
        }
        stage('Terraform Plan') {
            steps {
                dir("$WORKSPACE/pdp-project/eks/eks-cluster") {
                    sh '''
                    terraform init
                    terraform plan
                    '''
                }
              input 'Do you want to Apply changes?'
            }
        }
        stage('Terraform Apply') {
            steps {
                dir("$WORKSPACE/pdp-project/eks/eks-cluster") {
                    sh '''
                    terraform apply --auto-approve
                    '''
                }
            }
        }

    }
}