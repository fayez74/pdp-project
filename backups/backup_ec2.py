import boto3
import datetime

def handler(event,context):

    ec2  = boto3.resource('ec2',region_name='eu-west-1')

    #Filter out instances using tags

    instanceIds = [] 
    volumeIds = []

    date = datetime.date.today()
    now = datetime.datetime.now()

    stateFilter = {
    "Name": 'instance-state-name',
    "Values": ['running']
    }
    tagsFilter = {
        "Name": 'tag:Name',
        "Values": ['jenkins']
    }

    for instance in ec2.instances.filter(Filters=[tagsFilter,stateFilter]):
        instanceIds.append(instance.id)

    #Stop the instances
        for each_instance in instanceIds:
            print("'{}' found".format(each_instance))
            print("Stopping '{}'".format(each_instance))

            ec2_instance = ec2.Instance(each_instance)
            ec2_instance.stop()
            ec2_instance.wait_until_stopped()

            print("'{}' has been stopped".format(each_instance))

    #Check what volumes are attached to those instances
            ec2_client = boto3.client('ec2', region_name='eu-west-1')
            response = ec2_client.describe_instances(InstanceIds=[each_instance])

            for reservations in response['Reservations']:
                for instances in reservations['Instances']:
                    for blockDeviceMappings in instances['BlockDeviceMappings']:
                        volumeIds.append(blockDeviceMappings['Ebs']['VolumeId'])

            snapshotIds = []
    #Create snapshot from those volumes
            for each_volume in volumeIds:
                print("Taking snapshot for '{}'".format(each_volume))
                volumeId_response = ec2_client.create_snapshot(
                    Description="Snapshot created using backup_ec2.py",
                    VolumeId=each_volume,
                    TagSpecifications=[
                        {
                            'ResourceType':'snapshot',
                            'Tags': [
                                {
                                    'Key': 'Name',
                                    'Value': "jenkins-backup-{}-{}".format(date,now.strftime("%H:%M:%S"))
                                },
                            ]
                        },
                    ],
                )
                snapshotIds.append(volumeId_response.get('SnapshotId'))
            waiter = ec2_client.get_waiter('snapshot_completed')
            waiter.wait(SnapshotIds=snapshotIds) 

            for eachSnap in snapshotIds:
                print("Successfully created snapshot: {} ".format(eachSnap))

    # Restart instance
            print("Restarting instances: {}".format(each_instance))
            ec2_instance = ec2.Instance(each_instance)
            ec2_instance.start()
            ec2_instance.wait_until_running()

            print("Restart complete for: {} !".format(each_instance))

#handler('test','test')

