resource "aws_cloudwatch_event_rule" "backup_ec2_event_rule" {
  name        = "backupEc2"
  description = "Weekly snapshot at 12.00AM - UTC"
  schedule_expression = "cron(0 0 ? * 6 *)"
  is_enabled = "true"
}

resource "aws_cloudwatch_event_target" "backup_ec2" {
  target_id = "ebs_unencrypted_volume_or_snapshot"
  rule      = aws_cloudwatch_event_rule.backup_ec2_event_rule.name
  arn       = aws_lambda_function.test_lambda.arn
}

resource "aws_lambda_permission" "allow_execution_from_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.backup_ec2_event_rule.arn
}