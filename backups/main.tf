provider "aws" {
    region =  "eu-west-1"
}

data "terraform_remote_state" "vpc_infrastructure" {
        backend = "s3"
        config = {
          bucket = var.remote_state_bucket
          key = "terraform/vpc/terraform.tfstate"
          region = "eu-west-2"
          
         }
  
}

resource "aws_lambda_function" "test_lambda" {
  filename      = "backup_ec2.zip"
  function_name = "backup_ec2"
  role          = aws_iam_role.lambda_role.arn
  handler       = "backup_ec2.handler"
  source_code_hash = filebase64sha256("backup_ec2.zip")
  timeout       = "240"  
  runtime       = "python3.7"
  publish       = "true"

}

